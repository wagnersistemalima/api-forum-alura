# api-forum-alura

* Adicionar o Spring Data JPA na API, incluindo suas dependências no arquivo pom.xml;
* Configurar o Spring Data JPA via propriedades no arquivo application.yml;
* Mapear as classes de domínio como entidades JPA, utilizando as anotações @Entity, @Id, @ManyToOne e @OneToMany;
* Criar interfaces repository para acesso ao banco de dados, herdando da interface JpaRepository do Spring Data JPA;
* Testar as mudanças na API utilizando o Postman para envio de requisições HTTP.

## Adicionando plugin da jpa e no-arg

```
  <compilerPlugins>
	<plugin>spring</plugin>
	<plugin>jpa</plugin>
	<plugin>no-arg</plugin>
  </compilerPlugins>

```

## Adicionando dependecia noarg

```
<dependency>
	<groupId>org.jetbrains.kotlin</groupId>
	<artifactId>kotlin-maven-noarg</artifactId>
	<version>${kotlin.version}</version>
</dependency>

```

## motivaçoes sobre migrations / Banco de dados da api

* Definir o schema de criação de tabelas
* Como realizar evoluçoes nas tabelas
* Como reverter a alteraçao

## Solução migrations

* Cada mudança e realizada em um script sql (migration)
* Uma tabela no banco de dados registra a execução dos scripts
* Uma ferramenta automatiza o processo
* Utilizando a ferramenta Flyway
* Adicionar dependencia do flyway
```
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
</dependency>

```

* Com a dependencia do flyway adicionada no pom.xml, o spring boot reconhece a ferramenta e automaticamente 
o hibernate deixa de gerenciar o banco de dados, sendo assim teremos um controle maior
sobre o esquema do banco

## Organização de pacotes para migrations do banco e scripts

![alter text](/image/db-migration.png)

## Métodos de consultas com filtros nos repositórios da API

* utilizando o padrão de nomenclatura findBy do Spring Data JPA
* findByCursoNome
* Filtrar na tabela Topico, atributo curso, Tabela Curso, atributo nome

```
@Repository
interface TopicoRepositoy: JpaRepository<Topico, Long > {

    fun findByCursoNome(nomeCurso: String, paginacao: Pageable): Page<Topico>
}
```

## Paginação

* Como realizar paginação e ordenação nas consultas ao banco de dados,
utilizando a interface Pageable do Spring Data JPA
* Recurso pronto no Spring Boot para realizar a paginação
* Pageable (org.springframework.data.domain)
* Classe do propio Spring

```
Pageable
```

## Ordenação

* parâmetros default de paginação e ordenação com a utilização
da anotação @PageableDefault
* Pageable tem suporte para ordenação
* Exemplo: Default quando não passar parametro na url

```
// ordenando 10 registros por pagina (size = 10)
// pela data de criação, (sort = ["data_criacao"])
// os mais recentes,  (direction = Sort.Direction.DESC)

@GetMapping
    fun listar(
        @RequestParam(required = false) nomeCurso: String?,
        @PageableDefault(size = 10, sort = ["dataCriacao"], direction = Sort.Direction.DESC) paginacao: Pageable

    ): Page<TopicoView> {

        return topicoService.listar(nomeCurso, paginacao)
    }

```
## cache do Spring.

* o Spring Boot tem um módulo exatamente para nós trabalharmos com essa parte de cache.
* Adicionar dependencia no pom.xml

```
<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-cache</artifactId>
</dependency>

```

* utilizar um cache em memória mesmo, utilizando um mapa. Só que isso é para ser
utilizado apenas em ambiente de desenvolvimento.

* Se você for colocar essa aplicação em produção, de fato, além da dependência
do starter cache, você vai ter que baixar também a dependência da ferramenta que
você quer utilizar de cache. Como, por exemplo, o Redis, o Memcached, ou qualquer
ferramenta que você queira utilizar como provedor de cache.

* Para habilitar o uso do cach na aplicação, adicionar a anotação na classe da aplicação:
* @EnableCaching
```
@SpringBootApplication
@EnableCaching
class ForumApplication

fun main(args: Array<String>) {
	runApplication<ForumApplication>(*args)
}


```

* A realizar cache de consultas no banco de dados utilizando a anotação
@Cacheable em métodos dos controllers
* @Cacheable(value = ["topicos"]) no metodo que quiser guadar em memoria o retorno
* Exemplo na classe Controler, no end point "/topicos"


````
    @GetMapping
    @Cacheable(value = ["topicos"])
    fun listar(
        @RequestParam(required = false) nomeCurso: String?,
        @PageableDefault(size = 10, sort = ["dataCriacao"], direction = Sort.Direction.DESC) paginacao: Pageable

    )

````

## Limpar o cash

* Usar a anotação @CacheEvict(value = ["topicos"]) 
* Limpar todos os registros que estão no cash sar (allEntries = true)

```
    @PostMapping
    @CacheEvict(value = ["topicos"], allEntries = true)
    fun cadastrar(@RequestBody @Valid form: NovoTopicoForm)
```

## Exemplo adicionando pom.xml outro banco de dados

* No entanto, é totalmente possível utilizar outros bancos de dados na API,
* como o MySQL, PostgreSQL, dentre outros, bastando para isso substituir no
* pom.xml a dependência do H2 e as configurações no arquivo application.yml
* MySQL

```
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>

```

* Configuraçao do application.yml

```
spring:
  datasource:
    driverClassName: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost/forum
    username: root
    password: root
  jpa:
    database-platform: org.hibernate.dialect.MySQL8Dialect

```


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/wagnersistemalima/api-forum-alura.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/wagnersistemalima/api-forum-alura/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

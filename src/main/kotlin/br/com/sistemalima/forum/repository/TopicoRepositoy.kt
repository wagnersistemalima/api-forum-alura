package br.com.sistemalima.forum.repository

import br.com.sistemalima.forum.dto.TopicoPorCategoriaDTO
import br.com.sistemalima.forum.model.Topico
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TopicoRepositoy: JpaRepository<Topico, Long > {

    fun findByCursoNome(nomeCurso: String, paginacao: Pageable): Page<Topico>

    @Query(value = "SELECT new br.com.sistemalima.forum.dto.TopicoPorCategoriaDTO(curso.categoria, count(t)) FROM Topico t JOIN t.curso curso GROUP BY curso.categoria")
    fun relatorio(): List<TopicoPorCategoriaDTO>
}
package br.com.sistemalima.forum.dto

data class TopicoPorCategoriaDTO(
    val categoria: String,
    val quantidade: Long
)

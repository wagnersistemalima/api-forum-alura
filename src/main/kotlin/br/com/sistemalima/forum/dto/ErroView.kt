package br.com.sistemalima.forum.dto

import java.time.LocalDateTime

data class ErroView(
    val timestamp: LocalDateTime,
    val status: Int,
    val error: String,
    val mensagem: String,
    val path: String
)



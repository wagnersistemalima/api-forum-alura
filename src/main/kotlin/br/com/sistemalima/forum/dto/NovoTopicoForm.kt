package br.com.sistemalima.forum.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class NovoTopicoForm(

    @field:NotBlank
    @field:Size(min = 5, max = 100)
    val titulo: String,

    @field:NotBlank
    val mensagem: String,

    @field:NotNull
    val idCurso: Long,

    @field:NotNull
    val idAutor: Long
)

package br.com.sistemalima.forum.dto

import br.com.sistemalima.forum.model.StatusTopico
import br.com.sistemalima.forum.model.Topico
import java.time.LocalDateTime

data class TopicoView(
    val id: Long?,
    val titulo: String,
    val mensagem: String,
    val statusTopico: StatusTopico,
    val dataCriacao: LocalDateTime
){
    constructor(topico: Topico): this (topico.id, topico.titulo, topico.mensagem, topico.status, topico.dataCriacao )
}

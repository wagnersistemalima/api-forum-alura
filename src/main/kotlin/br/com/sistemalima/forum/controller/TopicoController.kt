package br.com.sistemalima.forum.controller

import br.com.sistemalima.forum.dto.AtualizacaoTopicoForm
import br.com.sistemalima.forum.dto.NovoTopicoForm
import br.com.sistemalima.forum.dto.TopicoPorCategoriaDTO
import br.com.sistemalima.forum.dto.TopicoView
import br.com.sistemalima.forum.service.TopicoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/topicos"], produces = [MediaType.APPLICATION_JSON_VALUE])
class TopicoController(
    @Autowired
    private val topicoService: TopicoService
) {

    @GetMapping
    @Cacheable(value = ["topicos"])
    fun listar(
        @RequestParam(required = false) nomeCurso: String?,
        @PageableDefault(size = 10, sort = ["dataCriacao"], direction = Sort.Direction.DESC) paginacao: Pageable

    ): Page<TopicoView> {

        return topicoService.listar(nomeCurso, paginacao)
    }

    @GetMapping(path = ["/{id}"])
    fun buscarPorId(@PathVariable id: Long): TopicoView {
        return topicoService.buscarPorId(id)
    }

    @PostMapping
    @CacheEvict(value = ["topicos"], allEntries = true)
    fun cadastrar(@RequestBody @Valid form: NovoTopicoForm): ResponseEntity<TopicoView> {
        val topicoView = topicoService.cadastrar(form)
        val uri = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("{id}")
            .buildAndExpand(topicoView.id)
            .toUri()
        return ResponseEntity.created(uri).body(topicoView)
    }

    @PutMapping
    @CacheEvict(value = ["topicos"], allEntries = true)
    fun atualizar(@RequestBody @Valid form: AtualizacaoTopicoForm): ResponseEntity<TopicoView>{
        val topicoView = topicoService.atualizar(form)

        return ResponseEntity.ok(topicoView)
    }

    @DeleteMapping(path = ["/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @CacheEvict(value = ["topicos"], allEntries = true)
    fun deletar(@PathVariable id: Long) {
        topicoService.deletar(id)
    }

    @GetMapping(path = ["/relatorio"])
    fun relatorio(): ResponseEntity<List<TopicoPorCategoriaDTO>> {
        val relatorio = topicoService.relatorio()

        return ResponseEntity.ok(relatorio)
    }
}
package br.com.sistemalima.forum.controller

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
class HelloController {

    @GetMapping("/hello")
    fun hello(): String{
        return "Hello Word!"
    }
}
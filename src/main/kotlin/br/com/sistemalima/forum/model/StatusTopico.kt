package br.com.sistemalima.forum.model

enum class StatusTopico {

    NAO_RESPONDIDO,
    RESPONDIDO,
    SOLUCIONADO,
    FECHADO
}
package br.com.sistemalima.forum.service

import br.com.sistemalima.forum.constantes.ProcessResult
import br.com.sistemalima.forum.exceptions.NotFoundException
import br.com.sistemalima.forum.model.Curso
import br.com.sistemalima.forum.repository.CursoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CursoService(
    @Autowired
    private val cursoRepository: CursoRepository
) {

    @Transactional(readOnly = true)
    fun buscarPorId(id: Long): Curso {
        val curso = cursoRepository.findById(id).orElseThrow {
            NotFoundException("${ProcessResult.NOT_FOUND_EXCEPTION}, curso")
        }
        return curso

    }
}
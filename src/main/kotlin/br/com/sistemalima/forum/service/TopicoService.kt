package br.com.sistemalima.forum.service

import br.com.sistemalima.forum.constantes.ProcessResult
import br.com.sistemalima.forum.dto.AtualizacaoTopicoForm
import br.com.sistemalima.forum.dto.NovoTopicoForm
import br.com.sistemalima.forum.dto.TopicoPorCategoriaDTO
import br.com.sistemalima.forum.dto.TopicoView
import br.com.sistemalima.forum.exceptions.NotFoundException
import br.com.sistemalima.forum.mapper.TopicoFormMapper
import br.com.sistemalima.forum.mapper.TopicoViewMapper
import br.com.sistemalima.forum.repository.TopicoRepositoy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class TopicoService(

    @Autowired
    private val topicoViewMapper: TopicoViewMapper,

    @Autowired
    private val topicoFormMapper: TopicoFormMapper,

    @Autowired
    private val topicoRepository: TopicoRepositoy
) {

    @Transactional(readOnly = true)
    fun listar(nomeCurso: String?, paginacao: Pageable): Page<TopicoView> {

        val topicos = if (nomeCurso == null) {
            topicoRepository.findAll(paginacao)
        } else {
            topicoRepository.findByCursoNome(nomeCurso, paginacao)
        }

        return topicos.map { topico -> topicoViewMapper.map(topico) }

    }


    @Transactional(readOnly = true)
    fun buscarPorId(id: Long): TopicoView {
        val topico = topicoRepository.findById(id).orElseThrow {
            NotFoundException(ProcessResult.NOT_FOUND_EXCEPTION)
        }

        return topicoViewMapper.map(topico)

    }

    @Transactional
    fun cadastrar(novoTopicoForm: NovoTopicoForm): TopicoView {

        val topico = topicoFormMapper.map(novoTopicoForm)
        topicoRepository.save(topico)
        return topicoViewMapper.map(topico)
    }

    @Transactional
    fun atualizar(form: AtualizacaoTopicoForm): TopicoView {

        val topico = topicoRepository.findById(form.id).orElseThrow {
            NotFoundException(ProcessResult.NOT_FOUND_EXCEPTION)
        }

        val topicoAtualizado = topico.copy(
            titulo = form.titulo,
            mensagem = form.mensagem,
        )
        topicoRepository.save(topicoAtualizado)
        return topicoViewMapper.map(topicoAtualizado)

    }

    @Transactional
    fun deletar(id: Long) {

        val topico = topicoRepository.findById(id).orElseThrow {
            NotFoundException(ProcessResult.NOT_FOUND_EXCEPTION)
        }
        topicoRepository.delete(topico)
    }

    @Transactional(readOnly = true)
    fun relatorio(): List<TopicoPorCategoriaDTO> {
        return topicoRepository.relatorio()
    }
}
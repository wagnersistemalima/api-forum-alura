package br.com.sistemalima.forum.service

import br.com.sistemalima.forum.constantes.ProcessResult
import br.com.sistemalima.forum.exceptions.NotFoundException
import br.com.sistemalima.forum.model.Usuario
import br.com.sistemalima.forum.repository.UsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UsuarioService(
    @Autowired
    private val usuarioRepository: UsuarioRepository
) {

    @Transactional(readOnly = true)
    fun buscarPorId(id: Long): Usuario {
        val usuario = usuarioRepository.findById(id).orElseThrow {
            NotFoundException("${ProcessResult.NOT_FOUND_EXCEPTION}, autor")
        }
        return usuario
    }
}
package br.com.sistemalima.forum.exceptions

import java.lang.RuntimeException

class NotFoundException(mensagem: String): RuntimeException(mensagem) {
}
package br.com.sistemalima.forum.mapper

interface Mapper<T, U> {

    fun map(t: T): U

}

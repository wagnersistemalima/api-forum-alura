package br.com.sistemalima.forum.mapper

import br.com.sistemalima.forum.dto.TopicoView
import br.com.sistemalima.forum.model.Topico
import org.springframework.stereotype.Component

@Component
class TopicoViewMapper: Mapper<Topico, TopicoView> {

    override fun map(t: Topico): TopicoView {
        return TopicoView(
            id = t.id,
            titulo = t.titulo,
            mensagem = t.mensagem,
            statusTopico = t.status,
            dataCriacao = t.dataCriacao

        )
    }

}

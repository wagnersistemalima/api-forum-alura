package br.com.sistemalima.forum.mapper

import br.com.sistemalima.forum.dto.NovoTopicoForm
import br.com.sistemalima.forum.model.Topico
import br.com.sistemalima.forum.service.CursoService
import br.com.sistemalima.forum.service.UsuarioService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TopicoFormMapper(

    @Autowired
    private val cursosService: CursoService,

    @Autowired
    private val usuarioService: UsuarioService

): Mapper<NovoTopicoForm, Topico > {

    override fun map(t: NovoTopicoForm): Topico {
        val curso = cursosService.buscarPorId(t.idCurso)
        val autor = usuarioService.buscarPorId(t.idAutor)
        return Topico(
            titulo = t.titulo,
            mensagem = t.mensagem,
            curso = curso,
            autor = autor
        )
    }


}